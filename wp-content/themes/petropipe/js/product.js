jQuery(document).ready(function ($) {
  'use strict';

  var carousel = $('#product-gallery')

  carousel.carouFredSel({
    responsive: true,
    pagination: '.carousel-pagination',
    auto: false,
    items: {
      width: 260,
      visible: {
        min: 1,
        max: 3
      }
    },

    prev: {
      button: ".carousel-prev"
    },
    next: {
      button: ".carousel-next"
    },
    scroll: {
      items: 1
    },
    onCreate: function () {
      $(window).on('load resize', fixHeight);
      fixHeight();
    }
  });

  $('a[data-lightbox-gallery]').nivoLightbox();

  function fixHeight() {
    carousel.parent().add(carousel).height(carousel.children().first().height());
  }
});
