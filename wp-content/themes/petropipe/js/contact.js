(function () {
    'use strict';

    var rtype = getParameterByName('rtype');

    if(rtype){
        location.hash = "#form";
    }

    jQuery(document).ready(function ($) {
        if(rtype){
            var subject = $('input[name="your-subject"]');

            $('input[name="your-name"]').focus();

            switch(rtype){
                case 'product':
                    var product = getParameterByName('product');
                    if(product) {
                        subject.val('Ficha Técnica de ' + product);
                    }
                    break;
            }
        }
    });

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
})();


