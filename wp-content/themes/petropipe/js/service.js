jQuery(document).ready(function ($) {
    'use strict';
    var
        $links = $(),
        cont = 0;

    $('.service-tile').each(function(){
        var lnks = $('a', this).attr({
            'data-lightbox-gallery': 'service' + cont++,
            'title': $('.page-box__title', this).text()
        });
        $links = $links.add(lnks);
    });

    $links.nivoLightbox();
});
