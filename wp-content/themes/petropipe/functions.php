<?php

register_sidebar(
	array(
		'name'          => _x( 'Product Sidebar', 'backend', 'petropipe' ),
		'id'            => 'product-sidebar',
		'description'   => _x( 'Sidebar on the products page.', 'backend', 'buildpress_wp' ),
		'class'         => 'sidebar',
		'before_widget' => '<div class="widget  %2$s  push-down-30">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="sidebar__headings">',
		'after_title'   => '</h4>'
	)
);


add_filter('query_vars', 'pp_add_query_vars');
function pp_add_query_vars($vars){
	$vars[] = "pp_pid";
	return $vars;
}

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

add_action('wp_enqueue_scripts', 'pp_enqueue_scripts');
function pp_enqueue_scripts(){
	if(is_singular('portfolio')){
		wp_enqueue_script('carouFredSel', get_stylesheet_directory_uri() . '/js/jquery.carouFredSel-6.2.1-packed.js', array(), null, true );

		wp_enqueue_script('nivo-lightbox', get_stylesheet_directory_uri() . '/js/nivo-lightbox.min.js', array(), null, true );
		wp_enqueue_style('nivo-lightbox-base', get_stylesheet_directory_uri().'/css/nivo-lightbox.css' );
		wp_enqueue_style('nivo-lightbox-default', get_stylesheet_directory_uri().'/css/nivo-themes/default/default.css' );

		wp_enqueue_script('petropipe-product', get_stylesheet_directory_uri() . '/js/product.js', array(), null, true );

	} else if(is_page('servicios')) {
		wp_enqueue_script('nivo-lightbox', get_stylesheet_directory_uri() . '/js/nivo-lightbox.min.js', array(), null, true );
		wp_enqueue_style('nivo-lightbox-base', get_stylesheet_directory_uri().'/css/nivo-lightbox.css' );
		wp_enqueue_style('nivo-lightbox-default', get_stylesheet_directory_uri().'/css/nivo-themes/default/default.css' );

		wp_enqueue_script('petropipe-service', get_stylesheet_directory_uri() . '/js/service.js', array(), null, true );

	} else if(is_page('contactanos')) {
		wp_enqueue_script('petropipe-contact', get_stylesheet_directory_uri() . '/js/contact.js', array(), null, true );
	}
}
