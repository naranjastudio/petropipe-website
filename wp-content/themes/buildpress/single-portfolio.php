<?php
/**
 * Single post page
 *
 * @package BuildPress
 */

get_header();

get_template_part( 'part-main-title' );
get_template_part( 'part-breadcrumbs' );


?>
	<div class="master-container">
		<div class="container">
			<div class="row">
				<main class="col-xs-12 col-md-9 col-md-push-3" role="main">

					<?php
					while ( have_posts() ) :
						the_post();
						?>

						<article <?php post_class( 'post-inner' ); ?>>
							<?php if ( 'generic-title' === get_theme_mod( 'projects_title_mode', 'generic-title' ) ) : ?>
								<h2 class="hentry__title"><?php echo get_the_title(); ?></h2>
							<?php endif; ?>

							<div class="row">
								<div class="col-xs-12">
									<div class="product-carousel">
										<div id="product-gallery" class="project__gallery">
											<?php
											$img_count = 0;
											while ( have_rows( 'project_gallery' ) ) {
												the_row();
												++$img_count;

												$image = get_sub_field( 'project_image' );

												printf(
													'<a class="product-image-thumb" data-lightbox-gallery="producto" href="%1$s"><img class="img-responsive  project__gallery-image" src="%2$s" alt="%3$s" width="%4$d" height="%5$d" /></a> ',
													$image['url'],
													$image['sizes']['project-gallery'],
													esc_attr( $image['title'] ),
													$image['sizes']['project-gallery-width'],
													$image['sizes']['project-gallery-height']
												);
											}
											?>
										</div>
										<a class="carousel-control carousel-prev" href="#"><i class="fa fa-chevron-left"></i></a>
										<a class="carousel-control carousel-next" href="#"><i class="fa fa-chevron-right"></i></a>

										<div class="carousel-pagination"></div>
									</div>
								</div>


							</div>
							<div class="row">
								<?php $has_thumb = has_post_thumbnail() && ($img_count > 2 || $img_count == 0); ?>
								<div class="col-xs-12<?php echo $has_thumb? ' col-md-6' : ''; ?>">
									<div class="hentry__content  project__content">
										<?php the_content(); ?>
                    <a class="btn btn-primary text-uppercase" style="margin-top: 20px" href="/contactanos?rtype=product&product=<?php echo urlencode(get_the_title()); ?>">Solicitar ficha técnica de este producto</a>
                  </div>
								</div>
								<?php if ($has_thumb): ?>
								<div class="col-xs-12 col-md-6">
									<a href="<?php $_att = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); echo $_att[0] ?>" data-lightbox-gallery="producto">
										<?php the_post_thumbnail('project-gallery'); ?>
									</a>
								</div>
                <?php endif; ?>

                <div class="col-xs-12">
                    <div class="cta-inline">
                        <h4 class="alternative-heading">Solicitar cotización</h4>
                        <p>Solicítenos un presupuesto para su proyecto, coloque los datos completos en el formulario y uno de nuestros representantes se pondrá en contacto con usted.</p>
                        <?php echo do_shortcode('[contact-form-7 id="2541" title="Cotizaciones"]'); ?>
                    </div>
                </div>
							</div>
							<div class="clearfix"></div>
						</article>

					<?php
					endwhile;
					?>
				</main>
				<div class="col-xs-12  col-md-3  col-md-pull-9">
					<div class="sidebar">
						<?php dynamic_sidebar( 'product-sidebar' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>