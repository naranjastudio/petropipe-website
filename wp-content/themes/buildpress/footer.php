<?php
/**
 * Footer
 *
 * @package BuildPress
 */

$footer_widgets_num = (int)get_theme_mod( 'footer_widgets_num', 3 );

?>
	<footer>
		<?php if ( $footer_widgets_num > 0 ) : ?>
			<div class="footer">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'footer-widgets' ); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="footer-bottom">
			<div class="container">
				<div class="footer-bottom__left">
					<?php function auto_copyright($year = 'auto'){ ?>
						<?php if(intval($year) == 'auto'){ $year = date('Y'); } ?>
						<?php if(intval($year) == date('Y')){ echo intval($year); } ?>
						<?php if(intval($year) < date('Y')){ echo intval($year) . ' - ' . date('Y'); } ?>
						<?php if(intval($year) > date('Y')){ echo date('Y'); } ?>
					<?php } ?>
					<?php auto_copyright(2008); ?> © Petropipe de México, S.A. de C.V. Todos los derechos reservados.
				</div>
				<div class="footer-bottom__right">
					<a href="http://naranja-studio.com" style="opacity: .7" onmouseover="this.style.opacity=1" onmouseout="this.style.opacity=.7">
						<img src="/wp-content/themes/petropipe/img/zanahoria.svg" height="32" width="32">
					</a>
				</div>
			</div>
		</div>

	</footer>
	</div><!-- end of .boxed-container -->


	<?php wp_footer(); ?>
	</body>
</html>