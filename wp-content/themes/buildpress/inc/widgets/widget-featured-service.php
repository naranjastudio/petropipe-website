<?php
/**
 * Banner Widget
 *
 * @package BuildPress
 */

if ( ! class_exists( 'PT_Featured_Service' ) ) {
	class PT_Featured_Service extends WP_Widget {

		/**
		 * Length of the line excerpt.
		 */
		const INLINE_EXCERPT = 60;

		/**
		 * Register widget with WordPress.
		 */
		public function __construct() {
			parent::__construct(
				false, // ID, auto generate when false
				_x( 'PetroPipe: Featured Service' , 'backend', 'buildpress_wp'), // Name
				array(
					'description' => _x( 'Featured Service for Page Builder.', 'backend', 'buildpress_wp'),
					'classname'   => 'widget-featured-page',
				)
			);
		}

		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args
		 * @param array $instance
		 */
		public function widget( $args, $instance ) {

			$page_id        = absint( $instance['page_id'] );
			$layout         = sanitize_key( $instance['layout'] );
			$thumbnail_size = 'inline' === $layout ? 'thumbnail' : 'page-box';

			echo $args['before_widget'];

			if ( $page_id ) {
				$page_obj = new WP_Query( array( 'p' => $page_id, 'post_type' => 'any' ) );

				if ( $page_obj->have_posts() ) {
					$page_obj->the_post();
					?>

					<div <?php post_class( "page-box service-tile page-box--{$layout}" ); ?>>
						<a class="page-box__picture" href="<?php $_att = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); echo $_att[0] ?>"><?php echo the_post_thumbnail( $thumbnail_size ); ?></a>
						<div class="page-box__content">
							<h5 class="page-box__title  text-uppercase"><?php the_title(); ?></h5>
                            <div class="service-images">
                                <?php the_content(); ?>
                            </div>
						</div>
					</div>

					<?php

					wp_reset_postdata();
				}
			}
			else {
				echo _ex( 'Select page in widget settings', 'backend', 'buildpress_wp' );
			}

			echo $args['after_widget'];
		}

		/**
		 * Sanitize widget form values as they are saved.
		 *
		 * @param array $new_instance The new options
		 * @param array $old_instance The previous options
		 */
		public function update( $new_instance, $old_instance ) {
			$instance = array();

			$instance['page_id'] = absint( $new_instance['page_id'] );
			$instance['layout']  = sanitize_key( $new_instance['layout'] );

			return $instance;
		}

		/**
		 * Back-end widget form.
		 *
		 * @param array $instance The widget options
		 */
		public function form( $instance ) {
			$page_id = empty( $instance['page_id'] ) ? 0 : (int) $instance['page_id'];
			$layout  = empty( $instance['layout'] ) ? '' : $instance['layout'];

			?>

			<p>
				<label for="<?php echo $this->get_field_id( 'page_id' ); ?>"><?php _ex( 'Page:', 'backend', 'buildpress_wp'); ?></label> <br>
				<?php
                    _generate_post_select(
                        $this->get_field_id( 'page_id' ),
                        $this->get_field_name( 'page_id' ),
                        'essential_grid',  $page_id);
				?>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php _ex( 'Layout:', 'backend', 'buildpress_wp' ); ?></label> <br>
				<select id="<?php echo $this->get_field_id( 'layout' ); ?>" name="<?php echo $this->get_field_name( 'layout' ); ?>">
					<option value="block" <?php selected( $layout, 'block' ); ?>><?php _ex( 'With big picture', 'backend', 'buildpress_wp' ); ?></option>
					<option value="inline" <?php selected( $layout, 'inline' ); ?>><?php _ex( 'With small picture, inline', 'backend', 'buildpress_wp' ); ?></option>
				</select>
			</p>

			<p>
				How to change Image and Text for Featured Page can be found in our <a href="http://www.proteusthemes.com/docs/buildpress/#featured-page" target="_blank">Online Documentation</a>.
			</p>

			<?php
		}
	}
	add_action( 'widgets_init', create_function( '', 'register_widget( "PT_Featured_Service" );' ) );
}

function _generate_post_select($select_id, $select_name, $post_type, $selected = 0) {
    $post_type_object = get_post_type_object($post_type);
    $label = $post_type_object->label;
    $posts = get_posts(array('post_type'=> $post_type, 'post_status'=> 'publish', 'suppress_filters' => false, 'posts_per_page'=>-1));
    echo '<select name="'. $select_name .'" id="'.$select_id.'">';
    echo '<option value = "" >All '.$label.' </option>';
    foreach ($posts as $post) {
        echo '<option value="', $post->ID, '"', $selected == $post->ID ? ' selected="selected"' : '', '>', $post->post_title, '</option>';
    }
    echo '</select>';
}